# Вёрстка страниц сайта компании Keeng

## Коротко

Этот репозиторий содержит свёрстанные шаблоны страниц сайта компании Keeng.

## Демонстрационная вёрстка страниц

**[Акции. Подробно](https://apopheoz.ru/demo/keeng/akcii-podrobno.html)**

**[Акции. Разводящая](https://apopheoz.ru/demo/keeng/akcii-specpredlogeniya-razvod.html)**

**[Доставка и оплата](https://apopheoz.ru/demo/keeng/dostavka-oplata.html)**

**[Главная страница](https://apopheoz.ru/demo/keeng/index.html)**

**[Контакты](https://apopheoz.ru/demo/keeng/kontakty.html)**

**[Новости. Разводящая](https://apopheoz.ru/demo/keeng/novosti-razvod.html)**

**[О компании](https://apopheoz.ru/demo/keeng/o-kompanii.html)**

**[Отзывы](https://apopheoz.ru/demo/keeng/otzyvy.html)**

**[Список статей](https://apopheoz.ru/demo/keeng/spisok-statej.html)**

**[Статья. Подробно](https://apopheoz.ru/demo/keeng/statya-podrobno.html)**

**[Сотрудничество](https://apopheoz.ru/demo/keeng/sotrudnichestvo.html)**

**[Карточка модульного товара](https://apopheoz.ru/demo/keeng/kartochka-modulnogo-tovara.html)**

**[Одиночный товар](https://apopheoz.ru/demo/keeng/odinochnii-tovar.html)**

**[Избранное](https://apopheoz.ru/demo/keeng/spisok-izbrannogo.html)**

**[Каталог](https://apopheoz.ru/demo/keeng/spisok-tovarov.html)**

## Ссылки

**[apopheoz.ru](https://apopheoz.ru)** - лаборатория веб-дизайна «Апофеозъ»;

## Контактная информация

Отзывы и предложения: https://apopheoz.ru/feedback.html
